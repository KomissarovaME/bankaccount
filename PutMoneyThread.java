package org.kme.bank_account;

public class PutMoneyThread extends Thread{
    private BankAccount bankAccount;
    public PutMoneyThread(BankAccount bankAccount){
        this.bankAccount=bankAccount;
    }
    public void run(){
        for(int i=0;i<10;i++){
            bankAccount.putBalance(150);
            System.out.println("Зачислено 150p.");
            synchronized (bankAccount){
            bankAccount.notify();
        }}
    }
}
