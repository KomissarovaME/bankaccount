package org.kme.bank_account;

public class GetMoneyThread extends Thread {
    private BankAccount bankAccount;

    public GetMoneyThread(BankAccount bankAccount){
        this.bankAccount=bankAccount;
    }
    public void run(){
        for(int i=0;i<10;i++){
            synchronized (bankAccount) {
                if (bankAccount.getBalance() > 100) {
                    System.out.println("Снято 100р.");

                    bankAccount.takeOffMoney(100);
                } else {System.out.println("Ждём пока появятся деньги.");
                    try {
                        bankAccount.wait();
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }
}
