package org.kme.bank_account;

public class Launcher {
    public static void main(String[] args) {
        BankAccount bankAccount = new BankAccount("",0);
        PutMoneyThread putMoneyThread=new PutMoneyThread(bankAccount);
        GetMoneyThread getMoneyThread =new GetMoneyThread(bankAccount);
        putMoneyThread.start();
        getMoneyThread.start();
    }
}
