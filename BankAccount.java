package org.kme.bank_account;

public class BankAccount {
    private int balance;

    public BankAccount(String name, int balance) {
        this.balance = balance;
    }

    public void putBalance(int countMoney) {
        for (int i = 0; i < countMoney; i++) {
            balance++;
        }
    }
    public void takeOffMoney(int countMoney){
        for(int i=0;i<countMoney;i++){
            balance--;
        }
    }
    public int getBalance(){
        return balance;
    }
}
